# Starter #

Kod źródłowy instalatora Starter.exe.
W folderze StarterInstallator/bin/Debug znajduje się instalator .msi.
Pozwala on na wybór folderu instalacji oraz tworzy skrót w MenuStart.

# Uruchomienie #

Należy otworzyć wiersz poleceń, przejść do folderu instalacji i uruchomić: "Starter.exe NazwaUrządzenia".

Instrukcja uruchamiania Starter jako usługi przy starcie systemu znajduje się tutaj:
https://3controls.atlassian.net/wiki/spaces/TAN/pages/36995074/Starter
